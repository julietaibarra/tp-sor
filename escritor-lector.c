/*
*La condición de carrera entre los threads sucede en el momento en el proceso escritor no termino de escribir  y, simultaneamente, el proceso lector comienza a leer(es decir, ambos procesos manipulando una variable de forma concurrente).

*Cuando sólo un proceso accede a escribir y otro accede a leer los datos, los procesos estan en la sección critica( una parte de código que sólo proceso puede ingresar por vez).

*En este pseudocódigo si es necesario agregar semáforos de sincronización ya cuando un proceso modifica el valor del semáforos, ningun otro proceso puede modificar simultaneamente el valor del mismo.

*/

#include <stdio.h>
#include <stdlilb.h>
//#include <pthread.h>
#include <semaphore.h>
/*************************************/
//variables compartidas
Semaphore mutex1(1), mutex2(1);
Semaphore lector(1), escritor(1);
int numLector=0;
int numEscritor=0;
/************************************/
void* lector(void* parametro){
	while(true){
	lector.wait();//decrementa
	mutex1.wait();
	numLector++;
	if(numLector==1){
		escritor.wait();
		}
	mutex1.signal();//incrementa
	lector.signal();

	//Lectura del recurso
	mutex1.wait();
	numLectores--:
	if(numLector==0){
		ecritor.signal();
		}
	mutex1.signal();
/*************************************/
void escritor(){
	while(true){
	mutex2.wait();
	numEscritor++;
	if(numEscritor==1){
		lector.wait();
	}
	mutex2.signal();
	escritor.wait();

	//Escritura en el recurso
	escritor.signal();
	mutex2.wait();
`	numEscritor--;
	if(numEscritor==0){
		lector.signal();
	}
	mutex2.signal();
	}
}


